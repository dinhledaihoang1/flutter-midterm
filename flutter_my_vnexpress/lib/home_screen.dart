import 'package:flutter/material.dart';
import 'package:flutter_my_vnexpress/listwidget.dart';
import 'package:flutter_my_vnexpress/shared/listitem.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({ Key? key }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {

  List<ListItem> listTiles = [
    ListItem(
      "https://i-english.vnecdn.net/2022/05/19/cangcatlai-1652947201-16529472-6936-8877-1652950889_r_300x180.jpg",
      "US contributes to easing congestion at Vietnam's busiest container port",
      "Minh Nga",
      "May 19, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/19/hohoankiem76021616303680704416-9067-2085-1652935435_r_300x180.jpg",
      "Vietnam aims to be 50 percent urbanized by 2030",
      "Viet Tuan",
      "May 19, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/18/xjzqjxbdgrkkxfjn62csbyr63u-165-1816-5786-1652862678_r_300x180.jpg",
      "Vietnam launches national blockchain association",
      "Khuong Nha",
      "May 18, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/17/biden-1652790822-1652790835-7788-1652790942_r_300x180.jpg",
      "US appreciates Vietnam PM's contributions to US-ASEAN summit",
      "Thanh Danh",
      "May 18, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/17/abo1g4u3vt0ypxlb0xygq3czhvynos-5573-2801-1652777785_r_300x180.jpg",
      "School vice principal disciplined after forcing students to eat trashed food",
      "Anh Tuan",
      "May 17, 2022"
    ),
  ];

  List<ListItem> listTiles2 = [
    ListItem(
      "https://i-english.vnecdn.net/2022/05/28/1-1653727912-3163-1653727917_r_500x300.jpg",
      "90 pct of real estate brokers are unlicensed: realty association",
      "Vu Le",
      "May 28, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/27/1-1653641738-6044-1653641742_r_300x180.jpg",
      "VN-Index closes fourth gaining session",
      "Dat Nguyen",
      "May 27, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/27/WEBCangCatLai62-1653616402-2421-1653632420_r_300x180.jpg",
      "S&P upgrades Vietnam ratings",
      "Dat Nguyen",
      "May 27, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/26/wq3c57t6zriwzktrqg2m2eapwq-165-8739-9378-1653537972_r_300x180.jpg",
      "Investors regret bottom fishing as cryptocurrencies crash",
      "Bao Lam",
      "May 27, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/26/mercedesmaybachgls600116210396-3345-2411-1653532654_r_300x180.jpg",
      "Vietnam runs out of luxury cars",
      "Doan Dung, Thanh Nhan",
      "May 27, 2022"
    ),
  ];

  List<ListItem> listTiles3 = [
    ListItem(
      "https://i-english.vnecdn.net/2022/05/28/19-1653450518-1653705541_r_300x180.jpg",
      "Aerial shots of Binh Dinh add grandeur to daily life",
      "Huynh Phuong",
      "May 28, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/20/t7-1653017450-1653031374-1653031395_r_300x180.jpg",
      "Top 10 Vietnam hotels selected by Tripadvisor",
      "Nguyen Quy",
      "May 26, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/26/download1-1653537869-165353787-7260-3377-1653537950_r_300x180.jpg",
      "Hong Kong Airlines to resume flights to Vietnam after 2 years",
      "Hoang Phong",
      "May 26, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/25/shutterstock126621363115506325-4519-4387-1653464430_r_300x180.jpg",
      "Vietnam tourism development ranked among three biggest global improvers",
      "Bao Lam",
      "May 25, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/25/vietnamthailankhachtayjp728187-4720-6041-1653441775_r_300x180.jpg",
      "Vietnam reaps tourism gains from SEA Games",
      "Hoang Phong, Lan Huong",
      "May 25, 2022"
    ),
  ];

  List<ListItem> listTiles4 = [
    ListItem(
      "https://i-english.vnecdn.net/2022/05/25/7013c7b286f846a61fe9-jpeg-1653-5294-5818-1653467458_r_300x180.jpg",
      "Vietnam's patriotic love of sports",
      "Quynh Nguyen",
      "May 25, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/23/k72131586350677158651348063858-3207-3063-1653287531_r_300x180.jpg",
      "Stressed students lack safe place to vent concerns",
      "Duong Tam",
      "May 24, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/23/khangiaChi13647516097277431605-7732-9647-1653286494_r_300x180.jpg",
      "Lawmakers mull creating fund for cinema development",
      "Dang Khoa",
      "May 23, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/20/evan1jpeg32201653020738-165303-4916-1536-1653033691_r_300x180.jpg",
      "One-handed photojournalist leaves impression at SEA Games 31",
      "Hoang Ha",
      "May 20, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/17/1-1652754027-4202-1652754037_r_300x180.jpg",
      "Working remotely for foreign employers opens up dynamic lifestyle",
      "Dang Khoa, Linh Do",
      "May 17, 2022"
    ),
  ];

  List<ListItem> listTiles5 = [
    ListItem(
      "https://i-english.vnecdn.net/2022/05/28/liemch-1653720557-8986-1653720581_r_500x300.jpg",
      "Vietnam chess ace to compete in Prague Masters",
      "Xuan Binh",
      "May 28, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/28/parkjpeg64481653454864-1653639-4011-2442-1653699853_r_300x180.jpg",
      "Coach Park wants to reclaim AFF Cup title from Thailand",
      "Hoang Nguyen",
      "May 28, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/27/gianghuy-08-2-1-1145-165327343-7356-5277-1653641500_r_300x180.jpg",
      "VnExpress Marathon Hue 2023 opens for registration",
      "Thuy An",
      "May 27, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/27/jud3-jpeg-6517-1653618274-1653-3676-8700-1653619516_r_300x180.jpg",
      "Vietnamese judo's golden siblings draw strength from family, each other",
      "Lan Chi",
      "May 27, 2022"
    ),
    ListItem(
      "https://i-english.vnecdn.net/2022/05/26/Untitled34541653561789-1653568-4056-1710-1653568404_r_300x180.jpg",
      "Indonesian athlete hurls verbal abuse at Vietnamese SEA Games volunteers",
      "Duy Doan",
      "May 26, 2022"
    ),
  ];

  List<Tab> _tabList = [
    Tab(child: Text("News"),),
    Tab(child: Text("Business"),),
    Tab(child: Text("Travel"),),
    Tab(child: Text("Life"),),
    Tab(child: Text("Sports"),),
  ];

  late TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(vsync: this, length: _tabList.length);
  }
  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 110.0,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.menu,
            color: Colors.white,
          ),
        ),
        backgroundColor: Color.fromARGB(255, 160, 34, 79),
        centerTitle: true,
        title: Text("My VNExpress", style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          
        ),),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(30.0),
          child: TabBar(
            indicatorColor: Colors.white,
            labelColor: Colors.white,
            isScrollable: true,
            controller: _tabController,
            tabs: _tabList,
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              child: ListView.builder(
                itemCount: _tabList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {},
                    child: listWidget(listTiles[index]),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              child: ListView.builder(
                itemCount: _tabList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {},
                    child: listWidget(listTiles2[index]),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              child: ListView.builder(
                itemCount: _tabList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {},
                    child: listWidget(listTiles3[index]),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              child: ListView.builder(
                itemCount: _tabList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {},
                    child: listWidget(listTiles4[index]),
                  );
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Container(
              child: ListView.builder(
                itemCount: _tabList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {},
                    child: listWidget(listTiles5[index]),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
 